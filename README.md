# Julia for DS liveproject 1

## Boston task

Not done as Jupyter notebook as I wanted to try to do the task as a standalone
script. The script can be run using the following command:
```
julia --project=Boston -e 'using Boston; Boston.main()'
```

The pictures will be shown in a browser. Tested only on Windows 10 / julia
1.6.2.

## Adult task

The "Adult" task done in Jupyter notebook instide `Adult/` subdirectory.
