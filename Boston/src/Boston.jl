module Boston

using Arrow
using CSV
using DataFrames
using Downloads
using FreqTables
using Logging
using Measures
using Plots
using SHA
using StatsBase

DATA_URL = "https://archive.ics.uci.edu/ml/machine-learning-databases/housing/housing.data"
DATA_FILE = "housing.data"
EXPECTED_SHA = [
    0xad,
    0xfa,
    0x6b,
    0x6d,
    0xca,
    0x24,
    0xa6,
    0x3f,
    0xe1,
    0x66,
    0xa9,
    0xe7,
    0xfa,
    0x01,
    0xce,
    0xe4,
    0x33,
    0x58,
    0x57,
    0xd1,
]
COLUMNS = [
    :crim,
    :zn,
    :indus,
    :chas,
    :nox,
    :rm,
    :age,
    :dis,
    :rad,
    :tax,
    :ptratio,
    :b,
    :lstat,
    :medv,
]

function iscorrect(datafile, checksum)
    open(datafile) do f
        return SHA.sha1(f) == checksum
    end
end

function downloaddata(dataurl, datafile)
    if isfile(datafile)
        @warn "$(datafile) exists, skipping the download"
    else
        @info "Downloading from $(dataurl)..."
        Downloads.download(dataurl, datafile)
    end
end

function plothistograms(dataframe, suffix, rows, cols)
    width = cols * 400
    height = rows * 300
    plot(
        map(
            field -> histogram(
                dataframe[:, field],
                xlabel = "$(field)$(suffix)",
                legend = false,
                bottom_margin = 15mm,
            ),
            names(dataframe, Float64),
        )...,
        layout = grid(rows, cols),
        size = (width, height),
        reuse = false,
    )
end

function plotscatters(dataframe, against, suffix, rows, cols)
    width = cols * 400
    height = rows * 300
    plot(
        map(
            field -> scatter(
                dataframe[:, field],
                against,
                xlabel = "$(field)$(suffix)",
                legend = false,
                smooth = true,
                ms = 1,
                bottom_margin = 15mm,
            ),
            names(dataframe, Float64),
        )...,
        layout = grid(rows, cols),
        size = (width, height),
        reuse = false,
    )
end

# Ref https://en.wikipedia.org/wiki/Bootstrapping_(statistics)
function getci(data)
    resample = () -> rand(data, length(data))
    bootstrap = [mean(resample()) for _ = 1:1000]
    return (
        q5 = quantile(bootstrap, 0.05),
        mean = mean(data),
        q95 = quantile(bootstrap, 0.95),
    )
end

function yerrorplot(dataframe, category, field)
    stats = combine(groupby(dataframe, category, sort = true), field => (getci => AsTable))
    plot(
        getindex(stats, :, category),
        stats.mean,
        yerror = (stats.mean - stats.q5, stats.q95 - stats.mean),
        seriestype = :scatter,
        title = string(category),
        label = nothing,
    )
end

function main()
    downloaddata(DATA_URL, DATA_FILE)
    if !iscorrect(DATA_FILE, EXPECTED_SHA)
        error("Checksum mismatch, cannot proceed.")
    end
    @info "File $(DATA_FILE) is present and has correct checksum."

    df = DataFrame(
        CSV.File(
            "housing.data",
            delim = ' ',
            ignorerepeated = true,
            header = Boston.COLUMNS,
        ),
    )

    @info describe(df)

    nominal = names(df, Int64)
    continuous = names(df, Float64)

    for field ∈ nominal
        @info combine(nrow, groupby(df, field))
        @info proptable(df, field)
    end

    plotly()

    beforefiltering = plothistograms(df, "", 4, 3)
    display(beforefiltering)
    filter!(:medv => <(50.0), df)
    afterfiltering = plothistograms(df, " (filtered)", 4, 3)
    display(afterfiltering)

    corr = corkendall(Matrix(df))
    medvnum = findfirst(isequal("medv"), names(df))
    order = sortperm(corr[:, medvnum])

    hmap = heatmap(names(df)[order], names(df)[order], corr[order, order], reuse = false)
    display(hmap)

    varcorrs = DataFrame(var = names(df), corr = corr[:, medvnum])
    sortedvarcorrs = sort(varcorrs, :corr, by = abs)

    @info sortedvarcorrs

    scatters = plotscatters(df, df.medv, "", 4, 3)
    display(scatters)

    select!(df, Not(:b))

    transform!(
        df,
        :crim => ByRow(log),
        :dis => ByRow(log),
        :zn => ByRow(>(0)),
        renamecols = false,
    )

    aftertransform = plothistograms(df, " (transformed)", 5, 2)
    display(aftertransform)
    scattersaftertransform = plotscatters(df, df.medv, " (transformed)", 5, 2)
    display(scattersaftertransform)

    ciplots = plot(
        map(var -> yerrorplot(df, var, :medv), [:chas, :rad, :zn])...,
        layout = grid(3, 1),
        size = (1200, 1200),
        reuse = false,
    )

    display(ciplots)

    select!(df, Not(:rad))
    @info describe(df)

    Arrow.write("housing.arrow", df)

end

end # module
